import React from "react";
import Header from "./Header";
import Footer from "./Footer";
const Layout = ({ children }) => {
  return (
    <div className="w-screen h-auto flex flex-col ">
      <Header />
      <main className="mt-14 md:mt-20 px-4 md:px-16 py-4 w-full bg-white">
        {children}
      </main>
      <Footer />
    </div>
  );
};

export default Layout;

import React, { useState } from "react";
import Link from "next/link";
import Image from "next/image";
import {
  GiShoppingCart,
  GiHamburgerMenu,
  GiReceiveMoney,
} from "react-icons/gi";
import { GoSearch } from "react-icons/go";
import { FaFileInvoiceDollar, FaShoppingBag } from "react-icons/fa";
import {
  IoAirplaneOutline,
  IoHeartOutline,
  IoStarOutline,
} from "react-icons/io5";
import { GrClose } from "react-icons/gr";
import { TbUserExclamation } from "react-icons/tb";
import { RiCustomerService2Fill, RiFileList3Line } from "react-icons/ri";
import Logo from "../img/logo.png";
import User from "../img/user.png";
import { motion } from "framer-motion";

const CustomComponent = React.forwardRef(function CustomComponent(props, ref) {
  return <div className="flex items-center">{props.children}</div>;
});

const Header = () => {
  const [openMenu, setOpenMenu] = useState(false);
  const [user, setUser] = useState(null);
  return (
    <header className="fixed z-50 items-center w-screen p-3 px-4 md:p-5 bg-header-gradient ">
      {/* desktop  */}
      <div className="hidden  md:flex w-full items-center justify-between px-4">
        {/* brand */}
        <div className="hidden md:flex  cursor-pointer ">
          <Link href="/" className="flex">
            <CustomComponent>
              <Image
                className=" object-cover"
                width={100}
                height={100}
                src={Logo}
                alt="logo"
              />
              <p className="font-semibold tracking-wider text-lg uppercase">
                Beli.com
              </p>
            </CustomComponent>
          </Link>
        </div>

        {/* search */}
        <div className="w-full mx-14 flex  bg-white rounded-lg">
          <input
            type="text"
            className="w-full bg-transparent px-3 placeholder:text-sm text-black caret-primary"
            placeholder="Cari aja disini"
          />
          <button className="bg-color1 px-2 ">
            <GoSearch className="text-lg hover:scale-150 " />
          </button>
        </div>
        {/* navigasi */}
        {/* <div className=" flex">
          <button className="list-none">Category</button>
        </div> */}

        <motion.div
          whileTap={{ scale: 0.75 }}
          whileHover={{ scale: 1.2 }}
          className="flex items-center px-5 cursor-pointer"
        >
          <GiShoppingCart className="text-headingColor text-3xl" />
          <div className="w-5 h-5 bg-white relative -top-2 right-2 rounded-full flex items-center justify-center ">
            <p className="text-black text-xs">3</p>
          </div>
        </motion.div>
        <div className="flex">
          <div className="w-full flex gap-3 items-center justify-center">
            <motion.button
              whileTap={{ scale: 0.75 }}
              whileHover={{ scale: 1.2 }}
              className="text-sm bg-transparent border border-black text-white py-1 px-2  rounded-lg font-semibold"
            >
              Masuk
            </motion.button>{" "}
            <span className="text-gray-900 ">|</span>
            <motion.button
              whileTap={{ scale: 0.75 }}
              whileHover={{ scale: 1.2 }}
              className="text-sm bg-black text-white border border-black outline-1 py-1 px-2  rounded-lg font-semibold"
            >
              Daftar
            </motion.button>
          </div>
        </div>
      </div>

      {/* mobile */}
      <div className="flex items-center justify-between md:hidden w-full h-full py-2">
        {/* search */}
        <div className="w-full  flex  bg-white rounded-lg">
          <input
            type="text"
            className="w-full bg-transparent px-3 placeholder:text-sm text-black  caret-primary"
            placeholder="Cari aja disini"
          />
          <button className="bg-color1 px-2 ">
            <GoSearch className="text-lg hover:scale-150 " />
          </button>
        </div>

        <div className="flex items-center px-5">
          <GiShoppingCart className="text-headingColor text-2xl" />
          <div className="w-5 h-5 bg-white relative -top-2 right-2 rounded-full flex items-center justify-center ">
            <p className="text-black text-xs">3</p>
          </div>
        </div>
        <div className="flex">
          <GiHamburgerMenu
            className="text-xl"
            onClick={() => setOpenMenu(true)}
          />
        </div>
        {openMenu && (
          <div
            className={`flex-col fixed w-[100%] bg-white px-2 py-1 drop-shadow-lg h-screen top-0 right-0  ${
              openMenu
                ? "animate-[enter_0.2s_alternate]"
                : "animate-[quitt_0.5s_alternate]"
            }   `}
          >
            <div className="flex items-center justify-start ">
              <GrClose
                className="text-xl  text-black"
                onClick={() => setOpenMenu(false)}
              />
              <p>Menu Utama</p>
            </div>
            {user && (
              <div className="">
                <Image
                  src={User}
                  className="rounded-full"
                  width={50}
                  height={50}
                  alt="user"
                />
              </div>
            )}
            {!user && (
              <div className="w-full flex gap-3 items-center justify-between py-4">
                <motion.button
                  whileHover={{ scale: 1.2 }}
                  whileTap={{ scale: 0.75 }}
                  className="w-full text-sm bg-transparent border border-color1  py-1 px-2  rounded-lg font-semibold cursor-pointer"
                >
                  Masuk
                </motion.button>{" "}
                <span className="text-color1">|</span>
                <motion.button
                  whileHover={{ scale: 1.2 }}
                  whileTap={{ scale: 0.75 }}
                  className="w-full text-sm bg-color1 text-white  outline-1 py-1 px-2  rounded-lg font-semibold cursor-pointer"
                >
                  Daftar
                </motion.button>
              </div>
            )}

            <div className="flex flex-col justify-center gap-8 border-b py-5 border-gray">
              {/* aktivitas saya */}
              <div className="w-full flex flex-col gap-2  drop-shadow-lg">
                <p className="text-sm font-semibold">Aktivitas Saya</p>
                <motion.div
                  whileTap={{ scale: 0.9 }}
                  whileHover={{ scale: 1.2 }}
                  className="flex gap-1"
                >
                  <RiFileList3Line className="text-color1" />
                  <p className="text-sm">Daftar Transaksi</p>
                </motion.div>
                <motion.div
                  whileTap={{ scale: 0.9 }}
                  whileHover={{ scale: 1.2 }}
                  className="flex gap-1 "
                >
                  <IoHeartOutline className="text-color1" />
                  <p className="text-sm">Wishlist</p>
                </motion.div>
                <motion.div
                  whileTap={{ scale: 0.9 }}
                  whileHover={{ scale: 1.2 }}
                  className="flex gap-1"
                >
                  <IoStarOutline className="text-color1" />
                  <p className="text-sm">Ulasan</p>
                </motion.div>
              </div>

              {/* semua kategory */}
              <div className="w-full flex flex-col gap-2">
                <p className="text-sm font-semibold">Semua Kategory</p>
                <motion.div
                  whileTap={{ scale: 0.9 }}
                  whileHover={{ scale: 1.2 }}
                  className="flex gap-1"
                >
                  <FaShoppingBag className="text-color1" />
                  <p className="text-sm">Belanja</p>
                </motion.div>
                <motion.div
                  whileTap={{ scale: 0.9 }}
                  whileHover={{ scale: 1.2 }}
                  className="flex gap-1"
                >
                  <FaFileInvoiceDollar className="text-color1" />
                  <p className="text-sm">Top & Tagihan</p>
                </motion.div>
                <motion.div
                  whileTap={{ scale: 0.9 }}
                  whileHover={{ scale: 1.2 }}
                  className="flex gap-1"
                >
                  <IoAirplaneOutline className="-rotate-45 text-color1" />
                  <p className="text-sm">Travel & Entertaiment</p>
                </motion.div>
                <motion.div
                  whileTap={{ scale: 0.9 }}
                  whileHover={{ scale: 1.2 }}
                  className="flex gap-1"
                >
                  <GiReceiveMoney className="text-color1" />
                  <p className="text-sm">Keuangan</p>
                </motion.div>
              </div>

              {/* pusat bantuan */}
              <div className="w-full  flex flex-col gap-2">
                <p className="text-sm font-semibold">Pusat Bantuan</p>
                <motion.div
                  whileTap={{ scale: 0.9 }}
                  whileHover={{ scale: 1.2 }}
                  className="flex gap-1"
                >
                  <TbUserExclamation className="text-color1" />
                  <p className="text-sm">Pesanan Dikomplain</p>
                </motion.div>
                <motion.div
                  whileTap={{ scale: 0.9 }}
                  whileHover={{ scale: 1.2 }}
                  className="flex gap-1"
                >
                  <RiCustomerService2Fill className="text-color1" />
                  <p className="text-sm">Bantuan TokoTokoin Care</p>
                </motion.div>
              </div>
            </div>
          </div>
        )}
      </div>
    </header>
  );
};

export default Header;

import React, { useEffect, useRef } from "react";
import { motion } from "framer-motion";
import Image from "next/image";

const BestProductList = ({ data, flag, scrollValue }) => {
  const carousel = useRef();
  useEffect(() => {
    carousel.current.scrollLeft += scrollValue;
  }, [scrollValue]);

  const randomItem = data.sort(() => Math.random() - 0.5);
  return (
    <div
      ref={carousel}
      className={` w-full flex items-center gap-3 drop-shadow-lg    scroll-smooth   ${
        flag
          ? "overflow-x-scroll scrollbar-none"
          : "overflow-x-hidden flex-wrap justify-center"
      }`}
    >
      {randomItem &&
        randomItem.map((el) => (
          <motion.div
            initial={{ opacity: 0, x: -200 }}
            animate={{ opacity: 1, x: 0 }}
            exit={{ opacity: 0, x: -200 }}
            key={el.id}
            className=" h-[145px] min-w-[115px] md:w-150 md:min-w-[150px] rounded-lg p-2 backdrop-blur-lg  flex flex-col  items-center justify-between relative"
          >
            <motion.div
              whileHover={{ scale: 1.2 }}
              whileTap={{ scale: 0.75 }}
              className="flex flex-col items-center relative cursor-pointer"
            >
              <Image
                src={el.imageUrl}
                width="100"
                height={100}
                alt="category"
              />
            </motion.div>
            <p className="text-xs text-center">{el.name}</p>
          </motion.div>
        ))}
    </div>
  );
};

export default BestProductList;

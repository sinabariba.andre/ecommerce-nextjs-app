import React from "react";
import { IoHeartOutline, IoHomeOutline, IoImageOutline } from "react-icons/io5";
import { IoMdCheckboxOutline } from "react-icons/io";
import { RiFileList3Line } from "react-icons/ri";
const Footer = () => {
  return (
    <footer className="fixed w-full bottom-0  bg-white px-3 pb-1 ">
      <ul className="w-full md:hidden flex items-center justify-between drop-shadow-lg">
        <li>
          <IoHomeOutline className="text-3xl drop-shadow-lg" />
        </li>
        <li>
          <IoImageOutline className="text-3xl drop-shadow-lg" />
        </li>
        <li>
          <IoMdCheckboxOutline className="text-3xl drop-shadow-lg" />
        </li>
        <li>
          <IoHeartOutline className="text-3xl drop-shadow-lg" />
        </li>
        <li>
          <RiFileList3Line className="text-3xl drop-shadow-lg" />
        </li>
      </ul>
    </footer>
  );
};

export default Footer;

import React, { useEffect, useState } from "react";
import CategoryList from "./CategoryList";
import { MdChevronLeft, MdChevronRight } from "react-icons/md";
import { motion } from "framer-motion";
import data from "../data/data";

const Category = () => {
  const [scrollValue, setScrollValue] = useState(0);
  useEffect(() => {}, [scrollValue]);
  return (
    <div className="w-full h-[150px] md:h-auto flex flex-col items-center justify-center md:my-6 ">
      <section className="w-full my-3 ">
        <div className="w-full flex items-center justify-between ">
          <p className="hidden md:flex text-lg text-headingColor font-semibold capitalize relative before:absolute before:rounded-lg  before:w-16 before:h-1 before:-bottom-1 before:left-0 before:bg-color1 transition-all ease-in-out duration-100">
            Kategori
          </p>
          <div className="hidden md:flex gap-3 items-center">
            <motion.div
              whileTap={{ scale: 0.7 }}
              className="w-8 h-8 rounded-lg bg-color1 hover:bg-orange-500 cursor-pointer hover:shadow-lg flex items-center justify-center"
              onClick={() => setScrollValue(-1150)}
            >
              <MdChevronLeft className="text-lg text-black" />
            </motion.div>
            <motion.div
              whileTap={{ scale: 0.7 }}
              className="w-8 h-8 rounded-lg bg-color1 hover:bg-orange-500 cursor-pointer transition-all duration-100 ease-in-out hover:shadow-lg flex items-center justify-center"
              onClick={() => setScrollValue(1150)}
            >
              <MdChevronRight className="text-lg text-black" />
            </motion.div>
          </div>
        </div>
      </section>
      <CategoryList scrollValue={scrollValue} flag={true} data={data} />
    </div>
  );
};

export default Category;

import React, { useEffect, useRef } from "react";
import { motion } from "framer-motion";
import Image from "next/image";

const CategoryList = ({ data, flag, scrollValue }) => {
  const carousel = useRef();
  useEffect(() => {
    carousel.current.scrollLeft += scrollValue;
  }, [scrollValue]);
  return (
    <div
      ref={carousel}
      className={`w-full h-350 grid grid-rows-2 grid-flow-col  items-center gap-3 scroll-smooth    ${
        flag
          ? "overflow-x-scroll scrollbar-none"
          : "overflow-x-hidden flex-wrap justify-center"
      }`}
    >
      {/* desktop */}
      {data &&
        data.map((el) => (
          <motion.div
            initial={{ opacity: 0, y: -200 }}
            animate={{ opacity: 1, y: 0 }}
            exit={{ opacity: 0, y: -200 }}
            key={el.id}
            className="hidden  md:flex flex-col  items-center justify-between relative h-[145px] min-w-[115px] md:w-150 md:min-w-[150px] rounded-lg p-2 backdrop-blur-lg cursor-pointer "
          >
            <motion.div
              whileHover={{ scale: 1.2 }}
              whileTap={{ scale: 0.75 }}
              className="flex flex-col items-center relative cursor-pointer "
            >
              <Image
                src={el?.imageUrl}
                width="100"
                height={100}
                alt="category"
                className="hover:drop-shadow-custom rounded-full "
              />
            </motion.div>
            <p className="text-xs text-center">{el.name}</p>
          </motion.div>
        ))}

      {/* mobile */}
      {data &&
        data.map((el) => (
          <motion.div
            initial={{ opacity: 0, y: 200 }}
            animate={{ opacity: 1, y: 0 }}
            exit={{ opacity: 0, y: 200 }}
            key={el.id}
            className=" md:hidden  min-w-[70px] md:w-150 md:min-w-[150px]  flex-col   rounded-lg py-2  backdrop-blur-lg  flex  items-center justify-between relative"
          >
            <motion.div
              whileHover={{ scale: 1.2 }}
              whileTap={{ scale: 0.75 }}
              className="flex flex-col items-center relative "
            >
              <Image
                src={el?.icon}
                width="30"
                height={30}
                alt="category"
                className="drop-shadow-custom"
              />
            </motion.div>
            <p className="text-[10px] text-center">{el.name}</p>
          </motion.div>
        ))}
    </div>
  );
};

export default CategoryList;

import elektronik from "../img/category/elektronik.png";
import laptop from "../img/category/laptop.png";
import hp from "../img/category/hp.png";
import bajupria from "../img/category/bajupria.png";
import sepatupria from "../img/category/sepatupria.png";
import taspria from "../img/category/taspria.png";
import taswanita from "../img/category/taswanita.png";
import jamtangan from "../img/category/jamtangan.png";
import kesehatan from "../img/category/kesehatan.png";
import hobi from "../img/category/hobi.png";
import olahraga from "../img/category/olahraga.png";
import buku from "../img/category/buku.png";
import fotografi from "../img/category/fotografi.png";
import sepatuwanita from "../img/category/sepatuwanita.png";
import fmuslim from "../img/category/fmuslim.png";
import fbayi from "../img/category/fbayi.png";
import pakaianwanita from "../img/category/pakaianwanita.png";
import otomotif from "../img/category/otomotif.png";
import souvenir from "../img/category/souvenir.png";

function importAll(r) {
  let images = {};
  r.keys().map((item, index) => {
    images[item.replace("./", "")] = r(item);
  });
  return images;
}

export const icons = importAll(
  require.context("../img/icons/", false, /\.(png|jpe?g|svg)$/)
);

export const category = [
  { id: 1, name: "Elekronik", imageUrl: elektronik, icon: icons["icoTv.png"] },
  {
    id: 2,
    name: "Komputer & Aksesoris",
    imageUrl: laptop,
    icon: icons["icoPc.png"],
  },
  {
    id: 3,
    name: "Handphone & Aksesoris",
    imageUrl: hp,
    icon: icons["icoPhone.png"],
  },
  {
    id: 4,
    name: "Pakaian Pria",
    imageUrl: bajupria,
    icon: icons["icoTshirt.png"],
  },
  {
    id: 5,
    name: "Sepatu Pria",
    imageUrl: sepatupria,
    icon: icons["icoSneakers.png"],
  },
  {
    id: 6,
    name: "Tas Pria",
    imageUrl: taspria,
    icon: icons["icoSlingBag.png"],
  },
  {
    id: 7,
    name: "Tas Wanita",
    imageUrl: taswanita,
    icon: icons["icoWomanbag.png"],
  },
  {
    id: 8,
    name: "Aksesoris Fashion",
    imageUrl: fbayi,
    icon: icons["icoAsesoris.png"],
  },
  {
    id: 9,
    name: "Jam Tangan",
    imageUrl: jamtangan,
    icon: icons["icoWatch.png"],
  },
  {
    id: 10,
    name: "Kesehatan",
    imageUrl: kesehatan,
    icon: icons["icoMedic.png"],
  },
  {
    id: 11,
    name: "Hobi & Koleksi",
    imageUrl: hobi,
    icon: icons["icoGuitar.png"],
  },
  { id: 12, name: "Olahraga", imageUrl: olahraga, icon: icons["icoSport.png"] },
  { id: 13, name: "Buku", imageUrl: buku, icon: icons["icoBook.png"] },
  {
    id: 14,
    name: "Fotografi",
    imageUrl: fotografi,
    icon: icons["icoFotografi.png"],
  },
  {
    id: 15,
    name: "Otomotif",
    imageUrl: otomotif,
    icon: icons["icoOtomotif.png"],
  },
  {
    id: 16,
    name: "Sepatu Wanita",
    imageUrl: sepatuwanita,
    icon: icons["icoHighheels.png"],
  },
  {
    id: 17,
    name: "Fashion Muslim",
    imageUrl: fmuslim,
    icon: icons["icoFmuslim.png"],
  },
  {
    id: 18,
    name: "Fashion Bayi",
    imageUrl: fbayi,
    icon: icons["icoBayi.png"],
  },
  {
    id: 19,
    name: "Pakaian Wanita",
    imageUrl: pakaianwanita,
    icon: icons["icoWomanclothes.png"],
  },
  {
    id: 20,
    name: "Souvenir & Pesta",
    imageUrl: souvenir,
    icon: icons["icoSouvenir.png"],
  },
];

export default category;

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html,js}",
    "./src/pages/**/*.{js,ts,jsx,tsx}",
    "./src/components/**/*.{js,ts,jsx,tsx}",
    "./node_modules/tw-elements/dist/js/**/*.js",
  ],
  theme: {
    extend: {
      backgroundPosition: {
        left: "left",
        right: "right",
      },
      backgroundSize: {
        "200%": "200%",
      },
      backgroundImage: {
        headerBg: {
          linearGradient: ("135deg", "#FDEB71", "10%", "#F8D800", "100%"),
        },
      },
      gridTemplateColumns: {
        // Simple 16 column grid
        16: "repeat(16, minmax(0, 1fr))",

        // Complex site-specific column configuration
        footer: "200px minmax(900px, 1fr) 100px",
      },
      width: {
        150: "150px",
        190: "190px",
        225: "225px",
        275: "275px",
        300: "300px",
        340: "340px",
        350: "350px",
        375: "375px",
        460: "460px",
        656: "656px",
        880: "880px",
        508: "508px",
      },
      height: {
        80: "80px",
        150: "150px",
        225: "225px",
        300: "300px",
        340: "340px",
        370: "370px",
        420: "420px",
        510: "510px",
        600: "600px",
        650: "650px",
        685: "685px",
        900: "900px",
        "90vh": "90vh",
      },
      minWidth: {
        210: "210px",
        350: "350px",
        620: "620px",
      },
      screens: {
        sm: "640px",
        md: "768px",
        lg: "1024px",
        xl: "1280px",
        "2xl": "1536px",
      },
      colors: {
        blue: "#1fb6ff",
        purple: "#7e5bef",
        pink: "#ff49db",
        orange: "#ff7849",
        green: "#13ce66",
        yellow: "#ffc82c",
        "gray-dark": "#273444",
        gray: "#8492a6",
        "gray-light": "#d3dce6",
        headingColor: "#2e2e2e",
        textColor: "#515151",
        primary: "#F4C134",
        backdropColor: "rgba(211, 220, 230, 0.3)",
        color1: "#21b6a8",
        color2: "#fc6767",
      },
      fontFamily: {
        sans: ["Graphik", "sans-serif"],
        serif: ["Merriweather", "serif"],
      },
      dropShadow: {
        custom: "0 4px 3px rgba(0, 0, 0, 0.3)",
        "4xl": [
          "0 35px 35px rgba(0, 0, 0, 0.25)",
          "0 45px 65px rgba(0, 0, 0, 0.15)",
        ],
      },
      keyframes: {
        goyang: {
          from: {
            backgroundPosition: "left",
          },
          to: {
            backgroundPosition: "right",
          },
        },
        enter: {
          from: {
            transform: "translate(0px, 500px)",
          },
          to: {
            transform: "translate-y-0",
          },
        },
        quitt: {
          from: {
            transform: "translate-y-0",
          },
          to: {
            transform: "translate(0px, 500px)",
          },
        },
      },
    },
  },
  plugins: [require("tw-elements/dist/plugin"), require("tailwind-scrollbar")],
};
